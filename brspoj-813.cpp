#include <stdio.h>
#include <stdlib.h>

#define INF 0x3F3F3F3F

int indexIni = 0;
int indexFim = 0;
int maxSaldo = -INF;

void makeMaxSumVet(int partA, int partB, int* vecSaldo, int pos){
	vecSaldo[pos] = partA - partB;
}

int applykadane(int* vecSaldo, int endLimit){
	int soma = 0;
	int ini = 0;
	for(int i = 0; i < endLimit; i++){
		soma = soma + vecSaldo[i];
		if (soma >= maxSaldo){
			maxSaldo = soma;
			indexIni = ini;
			indexFim = i;
		}
		if (soma < 0){ //não entendo direito essa condição
			soma = 0;
			ini = i + 1;
		}
		
	}
	//printf("ini:%d fim: %d\n", indexIni, indexFim);
	return 0;
}

void printSaida(int testeNumber){
	printf("Teste %d\n", testeNumber);
	if(indexIni >= 0 && maxSaldo > 0){ //temos um resultado positivo
		printf("%d %d\n",indexIni+1, indexFim+1);
	}
	else{
		printf("nenhum\n");
	}
	printf("\n");
}

int main(int argc, char** argv){
	
	int partA;
	int partB;
	int lines;
	int testeNumber = 0;
	
	scanf("%d",&lines);
	while (lines != 0){ //ler enquanto linha != 0
		testeNumber++;
		//ler partidas
		//printf("vector size: %d\n", lines);
		
		int *vecSaldo = new int[lines]; 
		for (int i = 0; i < lines; i++){
			scanf("%d %d", &partA, &partB);
			makeMaxSumVet(partA, partB, vecSaldo, i);
		}

		applykadane(vecSaldo, lines);
		printSaida(testeNumber);
		scanf("%d",&lines); //ler enquanto linha != 0
		
		//limpar para próxima sequencia
		delete(vecSaldo);
		indexIni = 0;
		indexFim = 0;
		maxSaldo = -INF;
	}

	return 0;
}

