/*
 * UVA Problem 1230 
 * https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=3671
 */
 
#include <stdio.h>

long fastModExpo(long x, long y, long m){
	if (y == 0){
		return 1; // base case
	}
	long ans = fastModExpo(x, y/2, m);
	ans = ((ans % m) * (ans % m)) % m;
	if ( y % 2 == 1){ // m is odd
		ans = (ans * (x % m)) % m;
	} 
	return ans;
} // end of fastModExpo

int main(){
	int operations;
	scanf("%d", &operations);
	for (int i = 0; i < operations; i++){
		long x, y, m = 0;
		scanf("%ld", &x);
		scanf("%ld", &y);
		scanf("%ld", &m);

		long result = fastModExpo(x, y, m);
		printf("%ld\n", result);
	}//end of for
}//end of main