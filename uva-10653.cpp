/*
 * UVA 10653
 * https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=1594
 */

#include <stdio.h>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;
queue <int> xQueue;
queue <int> yQueue;
int** map;
int R;
int C;


void printMatrix(int R, int C, int startX, int startY){
	for (int i = 0; i < R; i++){
		for (int j = 0; j < C; j++){
			if (map[i][j] == R*C){
				printf("\t*");
			}
			else{
				printf("\t%d", map[i][j]);
			}
		}
		printf("\n");
	}
	printf("---------------------------------------------------------------------------------\n");
}

/* fill map with the cost of arriving at each position from x and y */
void fillMatrix(int** map, int startX, int startY, int xLimit, int yLimit){
	int currCost = 0;

	int dx[] = {0, 0, -1, 1};
	int dy[] = {1, -1, 0, 0};

	xQueue.push(startX);
	yQueue.push(startY);

	while (xQueue.size() > 0 && yQueue.size() > 0){

		int currX = xQueue.front();
		xQueue.pop();
		int currY = yQueue.front();
		yQueue.pop();

		int newX = 0;
		int newY = 0;
		for(int i = 0; i < 4 ; i++){
			newX = currX + dx[i];
			newY = currY + dy[i];

			if( ( newX >= 0 && newX < xLimit) && 
				( newY >= 0 && newY < yLimit) && 
				( map[newX][newY] != xLimit * yLimit) /*bomb*/){
				if (map[newX][newY] == 0){ // still not visited
					map[newX][newY] = map[currX][currY] + 1;
					
					xQueue.push(newX);
					yQueue.push(newY);
				} 
			}
		} // end of for
	} // end of while
} // end of fillMatrix

int main(){
/** read input **/
	R = 0;
	C = 0;
	scanf("%d %d\n", &R, &C);

	while( R != 0 && C != 0){ // map
		int rows = 0; // get number of lines with bombs
		scanf("%d\n", &rows );

		map = new int*[R];
		for (int i = 0; i < R; i++){
			map[i] = new int[C];
			for (int j = 0; j < C; j++){
				map[i][j] = 0;
			}
		}

		for (int i = 0; i < rows; i++){ //for each line
			int rowBomb = 0;
			int nBombs = 0;
			scanf("%d %d ", &rowBomb, &nBombs);

			for (int j = 0; j < nBombs; j++){
				int colBomb = 0;
				scanf("%d ", &colBomb);
				map[rowBomb][colBomb] = R * C; // the cost of using this position needs to be high
			}
		}
		int rowStart, colStart, rowEnd, colEnd = 0;
		scanf("%d %d\n", &rowStart, &colStart);
		scanf("%d %d\n", &rowEnd, &colEnd);

/** walk **/
		fillMatrix(map, rowStart, colStart, R, C);
		//printMatrix(R, C, rowStart, colStart);
		int cost = map[rowEnd][colEnd];
		printf("%d\n", cost);
		delete(map);
		scanf("%d %d\n", &R, &C); //next map
	} // end of while
} // end of main